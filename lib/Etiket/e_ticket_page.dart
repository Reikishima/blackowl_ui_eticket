// ignore_for_file: depend_on_referenced_packages

import 'package:responsive_framework/responsive_framework.dart';
import 'package:flutter/material.dart';
import 'package:uiprojectbo/widget/all_button_history.dart';
import 'package:uiprojectbo/widget/main_row_up_history.dart';
import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class Etiket extends StatelessWidget {
  const Etiket({super.key});

  @override
  Widget build(BuildContext context) {
     SizeConfig().init(context);
    return Scaffold(
        backgroundColor: AppColors.darkBlue,
        body: SingleChildScrollView(
          child: Container(
            width: SizeConfig.screenWidth,
            height: SizeConfig.screenHeight,
            color: AppColors.darkBackground,
            child: Column(children: <Widget>[
              SizedBox(
                width: SizeConfig.screenWidth,
                height: SizeConfig.vertical(10),
                child: Row(
                  children: [
                    ResponsiveRowColumn(
                      layout: ResponsiveRowColumnType.COLUMN,
                      children: [
                        ResponsiveRowColumnItem(
                          child: _rowup(context),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.COLUMN,
                children: [
                  ResponsiveRowColumnItem(
                    child: _button(context),
                  ),
                ],
              )
            ]),
          ),
        ),
    );
  }
}

/// Membuat sebuah UI Row bagian appBar
Widget _rowup(BuildContext context) {
  return SizedBox(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(8),
    child: const MainRowUpHistory(),
  );
}

/// Isi dari 3 button Availlable,Used,dan transfered
Widget _button(BuildContext context) {
  return SizedBox(
    child: Column(
      children:const  [
         AllButtonHistory()
      ],
    ),
  );
}