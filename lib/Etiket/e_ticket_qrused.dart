import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:uiprojectbo/utils/app_colors.dart';
import 'package:uiprojectbo/utils/size_config.dart';
// ignore: depend_on_referenced_packages
import 'package:uiprojectbo/widget/containereticket.dart';
import 'package:uiprojectbo/widget/green_info.dart';
import 'package:uiprojectbo/widget/main_row_up_qrused.dart';

class Etiketqrused extends StatelessWidget {
  const Etiketqrused({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Container(
          width: SizeConfig.screenWidth,
          height: SizeConfig.screenHeight,
          color: AppColors.darkBackground,
          child: _mainwrapper(context)),
    );
  }
}

/// Widget Main Wrapper
Widget _mainwrapper(BuildContext context) {
  return SingleChildScrollView(
    child: Align(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: _rowUp(context),
          ),
          ResponsiveRowColumnItem(
            child: _greenBar(context),
          ),
          ResponsiveRowColumnItem(
            child: _maindetail(context),
          ),
        ],
      ),
    ),
  );
}

/// Widget untuk UI bar back button,Text,dan Logo
Widget _rowUp(BuildContext context) {
  return SizedBox(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(8),
    child: const MainRowUp(),
  );
}

/// Widget GreenBar untuk status Used E-Ticket
Widget _greenBar(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
      top: SizeConfig.vertical(0.5),
    ),
    child: Container(
      width: SizeConfig.horizontal(95),
      height: SizeConfig.vertical(10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5), color: AppColors.greenInfo),
      child: const GreenInfo(),
    ),
  );
}

///Widget Main Detail
Widget _maindetail(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
      top: SizeConfig.vertical(1),
    ),
    child: Container(
        width: SizeConfig.horizontal(95),
        height: SizeConfig.vertical(75),
        decoration: BoxDecoration(
            color: AppColors.darkFlatGold,
            borderRadius: BorderRadius.circular(5)),
        child: _containerEticket(context)),
  );
}

/// Widget Container Detail E-Ticket
Widget _containerEticket(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
      top: SizeConfig.vertical(0.5),
    ),
    child: const ContainerEticket(),
  );
}