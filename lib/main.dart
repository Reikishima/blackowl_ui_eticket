import 'package:responsive_framework/responsive_framework.dart';
import 'package:flutter/material.dart';

import 'package:uiprojectbo/Etiket/e_ticket_page.dart';
// ignore: depend_on_referenced_packages
import 'package:get/get.dart';

void main() {
  runApp(
     GetMaterialApp(
       builder: (BuildContext context, child) => ResponsiveWrapper.builder(
        BouncingScrollWrapper.builder(context, child!),
        minWidth: 375,
        maxWidth: 812,
        breakpoints: const <ResponsiveBreakpoint>[
          ResponsiveBreakpoint.autoScale(375, name: MOBILE),
        ],
      ),
      home: const Etiket(),
    ),
  );
}

// class MyProject extends StatelessWidget {
//   const MyProject({super.key});
//   @override
//   Widget build(BuildContext context) {
//     SizeConfig().init(context);
//     return MaterialApp(
//       builder: (BuildContext context, child) => ResponsiveWrapper.builder(
//         BouncingScrollWrapper.builder(context, child!),
//         minWidth: 375,
//         maxWidth: 812,
//         breakpoints: const <ResponsiveBreakpoint>[
//           ResponsiveBreakpoint.autoScale(375, name: MOBILE),
//         ],
//       ),
//       initialRoute: "/",
//       routes: {
//         "/": (context) => const Etiket(),
//       },
//       debugShowCheckedModeBanner: false,
//     );
//   }
// }
