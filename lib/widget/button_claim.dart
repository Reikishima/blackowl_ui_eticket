import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class ButtonClaim extends StatelessWidget {
  const ButtonClaim({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return  ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.darkBackground,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
        onPressed: () {},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: SizeConfig.horizontal(2)),
              child: ImageIcon(
                const AssetImage('assets/icons/voucher.png'),
                color: AppColors.darkFlatGold,
                size: 30,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: SizeConfig.horizontal(2)),
              child: Text(
                'Claimed E-Ticket:',
                style: GoogleFonts.montserrat(
                    color: AppColors.darkFlatGold, fontWeight: FontWeight.w500),
              ),
            ),
            Text(
              '4/8',
              style: GoogleFonts.montserrat(
                  color: AppColors.darkFlatGold, fontWeight: FontWeight.w800),
            )
          ],
        ),
      );
  }
}