import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class MainRowUpHistory extends StatelessWidget {
  const MainRowUpHistory({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Image.asset('assets/icons/Back.png'),
            ),
            Text(
              'My E-Ticket',
              style: GoogleFonts.montserrat(
                  color: AppColors.whiteBackground,
                  fontWeight: FontWeight.w700,
                  fontSize: 18),
            )
          ],
        ),
        IconButton(
          onPressed: () {},
          icon: Image.asset('assets/images/logo_bo.png'),
        )
      ],
    );
  }
}