import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import '../Etiket/e_ticket_page.dart';
import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class MainRowUp extends StatelessWidget {
  const MainRowUp({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            _buttonback(context),
            _textmyeticket(context),
          ],
        ),
        _logobo(context)
      ],
    );     
  }
}

/// Widget Untuk Button Back App
Widget _buttonback(BuildContext context) {
  return IconButton(
    onPressed: () {
      Get.to(const Etiket());
    },
    icon: Image.asset('assets/icons/Back.png'),
  );
}

/// Widget Text My E-Ticket
Widget _textmyeticket(BuildContext context) {
  return Text(
    'My E-Ticket',
    style: GoogleFonts.montserrat(
        color: AppColors.whiteBackground,
        fontWeight: FontWeight.w700,
        fontSize: 18),
  );
}

/// Widget Logo BO
Widget _logobo(BuildContext context) {
  return Image.asset(
    'assets/images/logo_bo.png',
    width: 50,
    height: 50,
  );
}