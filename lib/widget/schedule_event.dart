import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class ScheduleEvent extends StatelessWidget {
  const ScheduleEvent({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _judulevent(context),
          _scheduleevent(context),
          _location(context),
        ],
      );
  }
}

///Widget Text Judul Event
Widget _judulevent(BuildContext context) {
  return Container(
    padding: EdgeInsets.only(left: SizeConfig.horizontal(1)),
    child: Row(
      children: [
        Expanded(
          child: Text(
            'Nada: 1 Rindu Untuk GIGI',
            style: GoogleFonts.montserrat(
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w700,
              fontSize: 14,
            ),
          ),
        ),
      ],
    ),
  );
}

///Widget schedule event
Widget _scheduleevent(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(85),
    height: SizeConfig.vertical(3),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(1),
              right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/reservation.png',
            width: 20,
            height: 20,
          ),
        ),
        Expanded(
          child: Text(
            'Sat, 12 Nov 2022 - Tue, 15 Nov 2022',
            style: GoogleFonts.montserrat(
                color: AppColors.darkBackground,
                fontSize: 12,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    ),
  );
}

///Widget Location event
Widget _location(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(70),
    height: SizeConfig.vertical(3),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(1),
              right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/location.png',
            width: 20,
            height: 20,
          ),
        ),
        Expanded(
          child: Text(
            'Black Owl PIK',
            style: GoogleFonts.montserrat(
                color: AppColors.basicDark,
                fontSize: 12,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    ),
  );
}