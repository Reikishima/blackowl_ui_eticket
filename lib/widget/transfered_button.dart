import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class TransferedButton extends StatelessWidget {
  const TransferedButton({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
      width: SizeConfig.horizontal(95),
      height: SizeConfig.vertical(17),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: AppColors.lightGold,
          ),
          onPressed: () {},
          child: _mainwrapper3(context)),
    );
  }
}

///Widget MainWrapper
Widget _mainwrapper3(BuildContext context) {
  return Container(
    decoration: BoxDecoration(boxShadow: [
      BoxShadow(
          color: AppColors.basicDark.withOpacity(0.5),
          spreadRadius: 20,
          blurRadius: 12,
          offset: const Offset(0, 10),
          blurStyle: BlurStyle.solid)
    ]),
    padding: EdgeInsets.only(bottom: SizeConfig.vertical(0.5)),
    width: SizeConfig.horizontal(90),
    height: SizeConfig.vertical(15),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: SizeConfig.vertical(1)),
          child: SizedBox(
            width: SizeConfig.horizontal(95),
            height: SizeConfig.vertical(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _nada1(context),
                _scheduletransfered(context),
                _locationtransfered(context),
              ],
            ),
          ),
        ),
        _transfered(context)
      ],
    ),
  );
}

///Widget Nada 1 Rindu
Widget _nada1(BuildContext context) {
  return Container(
    padding: EdgeInsets.only(left: SizeConfig.horizontal(1.5)),
    child: Row(
      children: [
        Text(
          'Nada: 1 Rindu Untuk GIGI',
          style: GoogleFonts.montserrat(
            color: AppColors.darkBackground,
            fontWeight: FontWeight.w700,
            fontSize: 14,
          ),
        ),
      ],
    ),
  );
}

///Widget Schedule
Widget _scheduletransfered(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(85),
    height: SizeConfig.vertical(3),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(1),
              right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/clock.png',
            width: 20,
            height: 20,
          ),
        ),
        Expanded(
          child: Text(
            'Sunday, 24 November 2022, 20.30',
            style: GoogleFonts.montserrat(
                color: AppColors.darkBackground,
                fontSize: 12,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    ),
  );
}

///Widget Location
Widget _locationtransfered(BuildContext context) {
  return SizedBox(
    width: SizeConfig.horizontal(70),
    height: SizeConfig.vertical(3),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              left: SizeConfig.horizontal(1),
              right: SizeConfig.horizontal(3.5)),
          child: Image.asset(
            'assets/icons/location.png',
            width: 20,
            height: 20,
          ),
        ),
        Expanded(
          child: Text(
            'Black Owl PIK',
            style: GoogleFonts.montserrat(
                color: AppColors.basicDark,
                fontSize: 12,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    ),
  );
}

///Widget Transfered
Widget _transfered(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
        left: SizeConfig.horizontal(66), bottom: SizeConfig.vertical(0.5)),
    child: Container(
      padding: EdgeInsets.only(
          left: SizeConfig.horizontal(2.5), top: SizeConfig.vertical(0.7)),
      width: SizeConfig.horizontal(24),
      height: SizeConfig.vertical(3),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(2), color: AppColors.basicDark),
      child: Text(
        'Transferred',
        style:
            GoogleFonts.montserrat(fontSize: 10, fontWeight: FontWeight.w400),
      ),
    ),
  );
}
